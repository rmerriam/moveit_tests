#!/usr/bin/env python

import rospy, sys, copy
from std_msgs.msg import Float32
import geometry_msgs.msg
from geometry_msgs.msg import Pose
import moveit_commander
from moveit_commander import MoveGroupCommander
import dynamic_reconfigure.client

class Moveit_interface:
    def __init__(self):
        self.arm = moveit_commander.MoveGroupCommander('manipulator')
        self.end_effector_link = self.arm.get_end_effector_link()
        # Set the reference frame for pose targets
        self.reference_frame = 'world'
        # Set the arm reference frame accordingly
        self.arm.set_pose_reference_frame(self.reference_frame)

        # Allow replanning to increase the odds of a solution
        self.arm.allow_replanning(True)
        #self.arm.allow_replanning(True)

        # Allow some leeway in position (meters) and orientation (radians)
        self.arm.set_goal_position_tolerance(0.03)
        self.arm.set_goal_orientation_tolerance(0.1)

        # Set trajectory execution ros parameters to disabled
        client = dynamic_reconfigure.client.Client('move_group/trajectory_execution/')
        params = { 'allowed_start_tolerance' : '0.0'}
        config = client.update_configuration(params)

        self.busy_finding_path = False
        self.new_path_received = False
        path_plan_sub = rospy.Subscriber("arm_plan_cmd", Pose, self.plan_callback)
        self.result_pub = rospy.Publisher('plan_result', Float32, queue_size=1)

    def get_moveit_current_joints(self):
        return self.arm.get_current_joint_values()

    def get_current_pose(self):
        return self.arm.get_current_pose(self.end_effector_link).pose

    def plan_callback(self, msg):
        if self.busy_finding_path:
            return
        self.busy_finding_path = True
        self.target_pose = msg
        self.new_path_received = True


    def find_cartesian_path(self):
        self.arm.clear_pose_targets()
        self.arm.set_start_state_to_current_state()
        print("target pose in moveit_interface: ", self.target_pose)
        #input("from moveit interface, examine pose and hit any key to continue")
        self.arm.set_pose_target(self.target_pose)

        waypoints = []
        # start with the current pose
        current_pose = self.get_current_pose()
        waypoints.append(current_pose)

        # next, the target pose
        wpose = geometry_msgs.msg.Pose()
        wpose.position = self.target_pose.position
        wpose.orientation = self.target_pose.orientation
        waypoints.append(copy.deepcopy(wpose))

        #We want the cartesian path to be interpolated at a resolution of 0.5 cm
        #which is why we will specify 0.01 as the eef_step in cartesian translation.
        #We will specify the jump threshold as 0.0, effectively disabling it.

        plan = self.arm.plan()
        fraction = 0.0
        (plan, fraction) = self.arm.compute_cartesian_path(
                waypoints,   # waypoints to follow
                0.01,        # eef_step
                0.0,         # jump_threshold = max radians allowed in a joint for a single step
                False)       # do not avoid collisions

        if len(plan.joint_trajectory.points) == 0:
            print("no cartesian trajectory found to goal")
            self.busy_finding_path = False
            return False

        #there are warnings created by the first point having a time_from_start = 0, so we move it up a bit
        plan.joint_trajectory.points[0].time_from_start.nsecs += 100

        if len(plan.joint_trajectory.points) > 1:
            plan.joint_trajectory.points[1].time_from_start.nsecs += 200
            #the first two points are the same and they have the same completion times, which leads to an error
            #so we get rid of the first point
            #plan.joint_trajectory.points.pop(0)

        #print('cartesian planning took', num_tries, 'tries and yielded a fraction = ', fraction)
        #print(' the number of waypoints = ', len(plan.joint_trajectory.points), ' longest plan length = ', longest_plan_length)
        #print('cartesian planning yielded a fraction = ', fraction, ' with ', len(plan.joint_trajectory.points), ' waypoints' )

        self.arm.execute(plan, wait=True)
        rospy.sleep(1.5)

        if fraction >= 1.0:
            print('cartesian move complete, using {0:3d} waypoints'.format(len(plan.joint_trajectory.points)))
        else:
            print("\n ********** cartesian planning is having trouble *********")
            print("fraction = {0:.2f} of {1:3d} waypoints, so we will try desperation corrections to try to complete the plan".format(fraction, len(plan.joint_trajectory.points)))
            print("did not compute a complete plan, here is what we got: ", plan)
        self.result_pub.publish(fraction)
        self.busy_finding_path = False


def main():
    rospy.init_node("ariac_moveit_interface_node_for_rud")
    mi = Moveit_interface()
    pub = rospy.Publisher('moveit_pose', Pose, queue_size=10)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        pub.publish(mi.get_current_pose())
        if mi.new_path_received:
            mi.find_cartesian_path()
            mi.new_path_received = False
        rate.sleep()

if __name__ == '__main__':
    main()
