
sudo apt-get install ros-melodic-moveit-coresudo apt-get install ros-melodic-moveit-core \
                       ros-melodic-moveit-kinematics \
                       ros-melodic-moveit-ros-planning \
                       ros-melodic-moveit-ros-move-group \
                       ros-melodic-moveit-planners-ompl \
                       ros-melodic-moveit-ros-visualization \
                       ros-melodic-moveit-simple-controller-manager




roslaunch moveit_setup_assistant setup_assistant.launch


roslaunch ur10_moveit demo.launch

roslaunch ur10_moveit moveit_rviz.launch config:=true


grep -R "world_joint" .



  <remap from="joint_states" to="ariac/arm1/joint_states" />
  <remap from="robot_description" to="ariac/arm1/robot_description" />


------
rostopic pub /ariac/arm1/arm/command trajectory_msgs/JointTrajectory    "{joint_names: \
        ['linear_arm_actuator_joint',  'shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint'], \
     points: [ \
{time_from_start: {secs: 2}, \
        positions: [0.0, 3.14,  -1.570,  2.14, 3.24, -1.59, 0.126]}, \
{time_from_start: {secs: 4}, \
        positions: [0.0, 3.45,  -0.75,  2.14, 3.24, -1.59, 0.126]}, \
{time_from_start: {secs: 6}, \
        positions: [0.0, 3.60,  -0.538,  2.14, 3.24, -1.59, 0.126]}, \
]}" -1


rostopic pub /ariac/arm1/arm/command trajectory_msgs/JointTrajectory    "{joint_names: \
        ['linear_arm_actuator_joint',  'shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint'], \
     points: [ \
{time_from_start: {secs: 1}, \
        positions: [0.0, 3.14,  -1.570,  2.14, 3.24, -1.59, 0.126]} \
        ]}" -1

rostopic pub /ariac/arm1/arm/command trajectory_msgs/JointTrajectory    "{joint_names: \
        ['linear_arm_actuator_joint',  'shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint'], \
     points: [ \
{time_from_start: {secs: 1}, \
        positions: [0.3, 3.45,  -0.75,  2.14, 3.24, -1.59, 0.126]} \
        ]}" -1

