# Repository on How to Work With MoveIt

I entered the 2018 and [2019 ARIAC](https://challenge.gov/a/buzz/challenge/999/ideas/top) competitions. Both times my efforts foundered on using the robotic arms. Similarly, MoveIt could have been used in the [NASA Space Robotics Challenge](https://ninesights.ninesigma.com/servlet/hype/IMT?userAction=Browse&documentId=e6ac0e5e4f00359bcfb0464d32048b03&templateName=&documentTableId=1008809492095626525) to control the arms on the Valkyrie robot. In that competition I was one of the 20 qualifying teams. 

I am going to get MoveIt working, even if I don't submit an entry to the ARIAC competition. There is always the possibility of ARIAC competitions in later years, or other robitics 
competitions, where knowing MoveIt will be important. I want to be prepared.

The ARIAC competition has two [Universal Robot UR10](https://www.universal-robots.com/products/ur10-robot/) robot arms. MoveIt appears to be the only choice for working with them, at least that I can find. I've worked through the [ARIAC tutorial](https://bitbucket.org/osrf/ariac/wiki/2019/tutorials/moveit_interface) for MoveIt but after it demonstrates how to do planning using RVIZ it leaves competitors to do the rest of MoveIt on their own. 

The [Wiki](https://bitbucket.org/rmerriam/moveit_tests/wiki/Home) explains how to use this workspace. 

**This not a _typical_ repository. The standard `git clone` command is insufficient. The command to use is:**

git clone --recurse-submodules https://rmerriam@bitbucket.org/rmerriam/moveit_tests.git src
