add_executable(ur10_model_tutorial src/ur10_model_tutorial.cpp)
target_link_libraries(ur10_model_tutorial ${catkin_LIBRARIES} ${Boost_LIBRARIES})

install(TARGETS ur10_model_tutorial DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})
install(DIRECTORY launch DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})
